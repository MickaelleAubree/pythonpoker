class Player(object):
    def __init__(self, name=None):
        self.name = name  # First
        self.chips = 0
        self.stake = 0
        self.stake_gap = 0
        self.cards = []
        self.score = []
        self.fold = False
        self.ready = False
        self.all_in = False
        self.list_of_special_attributes = []  # First
        self.win = False


A, B, C, D = Player("Micka"), Player("Kassy"), Player("Linda"), Player("Gabriel")

players = [A, B, C, D]


def establish_player_attributes(list_of_players_not_out):
    address_assignment = 0
    dealer = list_of_players_not_out[address_assignment]
    dealer.list_of_special_attributes.append("dealer")
    address_assignment += 1
    address_assignment %= len(list_of_players_not_out)
    small_blind = list_of_players_not_out[address_assignment]
    small_blind.list_of_special_attributes.append("small blind")
    address_assignment += 1
    address_assignment %= len(list_of_players_not_out)
    big_blind = list_of_players_not_out[address_assignment]
    big_blind.list_of_special_attributes.append("big blind")
    address_assignment += 1
    address_assignment %= len(list_of_players_not_out)
    first_actor = list_of_players_not_out[address_assignment]
    first_actor.list_of_special_attributes.append("first actor")
    list_of_players_not_out.append(list_of_players_not_out.pop(0))


establish_player_attributes(players)

for player in players:
    print(f"Player {player.name} has the attributes : {player.list_of_special_attributes}")

players = [D, A, C]

for player in players:
    player.list_of_special_attributes.clear()

establish_player_attributes(players)
print("D is out ")

for player in players:
    print(f"Player {player.name} has the attributes : {player.list_of_special_attributes}")

players = [C, A]
for player in players:
    player.list_of_special_attributes.clear()

establish_player_attributes(players)
print("B is out ")

for player in players:
    print(f"Player {player.name} has the attributes : {player.list_of_special_attributes}")
# player1 = Player("Micka")

# def __repr__(self):
#     name = self.name
#     return name
