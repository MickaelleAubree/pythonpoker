import tkinter as tk
from PIL import Image, ImageTk

from deck import Deck
from hand import Hand
from player import Player


class Windows:

    def __init__(self):
        self.window = tk.Tk()
        self.window_width = 1024
        self.window_height = 768
        self.window.title('Poker game')
        # Background color
        self.window.config(background="#33E23B")
        self.window.minsize(1024, 768)
        self.window.maxsize(1024, 768)
        # For the icon
        self.window.iconbitmap('./images/IconPoker.ico')
        # For the width et the height of the window
        self.window.geometry("{}x{}".format(self.window_width, self.window_height))

    def imager(self, photo, size_x, size_y):
        canvas = tk.Canvas(self.window, width=71, height=96)
        canvas.create_image(0, 0, anchor=tk.NW, image=photo)
        canvas.place(x=size_x, y=size_y)
        label = tk.Label(image=photo)
        label.image = photo

    def card_imager(self, player, pos_x, pos_y):
        deck_base = Deck()
        for j in range(len(player.cards)):
            for i in range(len(deck_base.cards)):
                if str(deck_base.cards[i]) == str(player.cards[j]):
                    filename = './images/pokerImages/{:02}.gif'.format(i + 1)
                    card_poker = Image.open(filename)
                    photo = ImageTk.PhotoImage(card_poker)
                    self.imager(photo, pos_x + j * 50, pos_y)
                    break

    def returned_card_imager(self, player, pos_x, pos_y):
        for j in range(len(player.cards)):
            filename = './images/returned_card.jpg'
            returned_card_poker = Image.open(filename)
            photo = ImageTk.PhotoImage(returned_card_poker)
            self.imager(photo, pos_x + j * 50, pos_y)

    def to_bet(self, player, ia1, ia2, ia3, flop):
        self.card_imager(ia1, ia1.position_x, ia1.position_y)
        self.card_imager(ia2, ia2.position_x, ia2.position_y)
        self.card_imager(ia3, ia3.position_x, ia3.position_y)
        # Ici on compare les cartes de player et des ias pour voir qui a gagné
        hand_player = player.player_to_hand(flop)
        hand_ia1 = ia1.player_to_hand(flop)

        # Quand la fonction hand_value (qui est static sera fini) alors on compara les valeurs retournée entre ias et
        # player

        if hand_player[0] > hand_ia1[0]:
            print("le joueur possède la carte la plus forte")
        elif hand_player[0] < hand_ia1[0]:
            print("l'ia possède la carte la plus forte")
        else:
            print("l'ia et le joueur ont la même carte la plus forte")

    def poker_window(self, player, ia1, ia2, ia3, flop):

        # For the quit button and the play button
        exit_button = tk.Button(self.window, text='Exit',
                                command=lambda: self.window.quit())
        exit_button.grid(row=0, column=0)
        # For the pictures of the cards
        self.card_imager(player, player.position_x, player.position_y)
        self.card_imager(flop, flop.position_x, flop.position_y)
        self.returned_card_imager(player, ia1.position_x, ia1.position_y)
        self.returned_card_imager(player, ia2.position_x, ia2.position_y)
        self.returned_card_imager(player, ia3.position_x, ia3.position_y)

        bet_button = tk.Button(self.window, text='Bet', width=15, height=2,
                               command=lambda: self.to_bet(player, ia1, ia2, ia3, flop)
                               )
        bet_button.place(x=player.position_x, y=player.position_y - 100)

        '''faire comparaison avec les combinaisons entre player et les ia
        si player gagne alors afficher une box : you won, voulez vous recommencez
        si cela marche alors revoir les règle de nommage etc pour un projet propre et enfin
        ajoutez les règles du texas hold, a voir sur mlb'''

        self.window.mainloop()
