from deck import Deck


def hand_value(hand_array):
    print(hand_array)
    # Fonction pour voir si ya paire, 2 paire etc..
    # return la valeur de la main sous forme de nombre


class Hand:
    hand_value = []

    def __init__(self, player, flop):
        self.__player = player
        self.__flop = flop

    @property
    def player(self):
        return self.__player

    @property
    def flop(self):
        return self.__flop

    def __str__(self):
        res = self.hand_value
        res.sort(reverse=True)
        return ''.join(str(res))
