from deck import Deck


class Player:

    def __init__(self, position_x, position_y):
        self.__cards = []
        self.__position_x = position_x
        self.__position_y = position_y

    def player_to_hand(self, flop):
        hand_value = []
        deck_base = Deck()
        for i in range(len(self.cards)):
            for j in range(len(deck_base.cards)):
                if str(self.cards[i]) == str(deck_base.cards[j]):
                    hand_value.append(j)
        for i in range(len(flop.cards)):
            for j in range(len(deck_base.cards)):
                if str(flop.cards[i]) == str(deck_base.cards[j]):
                    hand_value.append(j)
        return sorted(hand_value, reverse=True)

    @property
    def position_x(self):
        return self.__position_x

    @property
    def position_y(self):
        return self.__position_y

    @property
    def cards(self):
        return self.__cards
