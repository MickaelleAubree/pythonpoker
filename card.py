class Card:
    colors = ["Pique", "Trèfle", "Carreau", "Coeur"]
    num = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'valet', 'dame', 'roi', 'as']

    def __init__(self, value, color):
        self.__value = value
        self.__color = color

    def __str__(self):
        return "{} de {}".format(self.num[self.value], self.colors[self.color])

    def compare(self, card2):
        if self.value > card2.value:
            return self
        elif card2.value > self.value:
            return card2
        else:
            return 'Cards are equals'

    @property
    def value(self):
        return self.__value

    @property
    def color(self):
        return self.__color
