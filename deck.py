import random

from card import Card


class Deck:
    def __init__(self):
        self.__cards = []
        for j in range(len(Card.num)):
            for i in range(len(Card.colors)):
                card = Card(j, i)
                self.cards.append(card)

    def shuffler(self):
        random.shuffle(self.cards)

    def distribution_player(self, player):
        for i in range(2):
            player.cards.append(self.cards[-1])
            del self.cards[-1]

    def distribution_flop(self, flop):
        for i in range(3):
            flop.cards.append(self.cards[-1])
            del self.cards[-1]

    def __str__(self):
        res = []
        for i in self.cards:
            res.append(i.__str__())
        return '\n'.join(res)

    @property
    def cards(self):
        return self.__cards
