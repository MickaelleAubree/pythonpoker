import windows
from flop import Flop
from deck import Deck
from player import Player

if __name__ == '__main__':
    # Here we create the the deck, the player, the ias and the bet with their coordinates as arguments
    player = Player(487, 650)
    ia1 = Player(50, 350)
    ia2 = Player(487, 50)
    ia3 = Player(850, 350)
    flop = Flop(460, 350)
    deck = Deck()
    # We shuffle the deck
    deck.shuffler()
    # We distribute the cards
    deck.distribution_player(player)
    deck.distribution_player(ia1)
    deck.distribution_player(ia2)
    deck.distribution_player(ia3)
    deck.distribution_flop(flop)
    # For the graphic display
    window = windows.Windows()
    window.poker_window(player, ia1, ia2, ia3, flop)
