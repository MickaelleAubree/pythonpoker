class Flop:
    def __init__(self, position_x, position_y):
        self.__cards = []
        self.__position_x = position_x
        self.__position_y = position_y

    @property
    def position_x(self):
        return self.__position_x

    @property
    def position_y(self):
        return self.__position_y

    @property
    def cards(self):
        return self.__cards
